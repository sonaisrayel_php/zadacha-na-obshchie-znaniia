<?php

namespace ShopExpress\ApiClient;
require __DIR__ . '/vendor/autoload.php';

$apiKey = "lNwzuV_Gb";
$userLogin = "admin";
$apiUrl = "http://newshop.kupikupi.org/adm/api";

if ($_GET['page']) {
    $val1 = ($_GET['page'] * 10) - 10;
} else {
    $val1 = 0;
}

$ApiClient = new ApiClient($apiKey, $userLogin, $apiUrl);

$limit = [
    "start" => $val1,
    "limit" => 10
];

try {
    $orders = $ApiClient->get("orders",
        $limit
    );

    $all_orders = $orders['orders'];
} catch (Exception\NetworkException $e) {
} catch (Exception\RequestException $e) {
}

?>
<html>
<head>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<h2>Orders</h2>
<div class="pagination">
    <a href="?page=1">1</a>
    <a href="?page=2">2</a>
    <a href="?page=3">3</a>
    <a href="?page=4">4</a>
    <a href="?page=5">5</a>
    <a href="?page=6">6</a>
</div>
<table>
    <tr>
        <th>Order id</th>
        <th>Email</th>
    </tr>

    <?php
    foreach ($all_orders as $order) {
        echo
            "<tr><td>" . $order['order_id'] . "</td><td>" .
            $order['email'] . "</td></tr>";
    }
    ?>
</table>
</body>
</html>

